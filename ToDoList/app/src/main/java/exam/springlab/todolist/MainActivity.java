package exam.springlab.todolist;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements TextToSpeech.OnInitListener
{
    private DatabaseManager manager;
    private ListView notes;
    private NoteAdapter noteAdapter;
    private ArrayList<Note> noteList;
    private ArrayList<Note> dataNotes;
    private ArrayList<Note> fireNotes;
    private FloatingActionButton add;
    private int actionPos;
    private Bitmap image;
    private MediaStore.Audio audio;
    private TextToSpeech speech;
    private SpeakView speechEdit=null;
    private FirebaseFirestore store;
    private boolean cloud=false;
    private FirebaseApp app;
    private FirebaseAuth auth;

    //private AlertDialog.Builder dialogBuilder;
    //private AlertDialog dialog;

    private final int CAPTURE_IMAGE=0;
    private final int CAPTURE_AUDIO=1;
    private final int CREATE_IMAGE=2;
    private final int CREATE_AUDIO=3;
    private final int CAPTURE_SPEECH=4;
    private static final int LOCATION_PERMISSION_REQUEST = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notes=findViewById(R.id.notes);
        noteList=new ArrayList<Note>();
        dataNotes=new ArrayList<Note>();
        fireNotes=new ArrayList<Note>();
        add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                addNote();
            }
        });
        speech=new TextToSpeech(this,this);
        /*for(int i=0;i<10;i++)
        {
            LatLng position=new LatLng(new Random().nextDouble()*i,new Random().nextDouble()*i);
            noteList.add(new Note("prova " + i, new Date(System.currentTimeMillis()),position));
        }*/
        manager=new DatabaseManager(MainActivity.this);
        app=FirebaseApp.initializeApp(MainActivity.this);
        auth=FirebaseAuth.getInstance();
        store=FirebaseFirestore.getInstance();
        Cursor cursor=manager.get();
        while(cursor.moveToNext())
        {
            try
            {
                int index=cursor.getColumnIndexOrThrow(DatabaseContract.DESC);
                String desc=cursor.getString(index);
                index=cursor.getColumnIndexOrThrow(DatabaseContract.DEADLINE);
                Date date=new Date(cursor.getLong(index));
                index=cursor.getColumnIndexOrThrow(DatabaseContract.LAT);
                Double lat=cursor.getDouble(index);
                index=cursor.getColumnIndexOrThrow(DatabaseContract.LNG);
                Double lng=cursor.getDouble(index);
                LatLng position=new LatLng(lat,lng);
                /*index=cursor.getColumnIndexOrThrow(DatabaseContract.IMAGE);
                Bitmap image=cursor.getBlob(index);
                index=cursor.getColumnIndexOrThrow(DatabaseContract.AUDIO);
                MediaStore.Audio audio=cursor.getBlob(index);*/
                Note note=new Note(desc,date,position);
                //note.setImage(image);
                //note.setAudio(audio);
                dataNotes.add(note);
                noteList.add(note);
            }
            catch (IllegalArgumentException e)
            {
                Log.e(getString(R.string.error),e.getMessage());
            }
        }

        store.collection("notes").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
                {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task)
                    {
                        if(task.isSuccessful())
                        {
                            for(QueryDocumentSnapshot doc:task.getResult())
                            {
                                Map<String,Object> result=doc.getData();
                                String desc= (String) result.get("desc");
                                Date deadline= new Date((long)result.get("deadline"));
                                LatLng position= (LatLng) result.get("position");
                                byte[] bytes= (byte[]) result.get("image");
                                Bitmap image= (Bitmap) BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                                bytes= (byte[]) result.get("audio");
                                //manca conversione da byte ad audio
                                Note note=new Note(desc,deadline,position);
                                note.setImage(image);
                                noteList.add(note);
                                fireNotes.add(note);
                            }
                            readaptListView(noteList);
                        }
                    }
                });
        noteAdapter=new NoteAdapter(getApplicationContext(),R.layout.note_layout,noteList);
        notes.setAdapter(noteAdapter);
        notes.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent=new Intent(getApplicationContext(), NoteActivity.class);
                intent.putExtra(getString(R.string.desc),noteList.get(position).getDescription());
                intent.putExtra(getString(R.string.deadline),noteList.get(position).getDeadline());
                intent.putExtra(getString(R.string.image), noteList.get(position).getImage());
                //intent.putExtra(noteList.get(position).getAudio(),R.string.audio);
                //intent.putExtra(noteList.get(position).getPosition(),R.string.pos);
                startActivity(intent);
            }
        });
        notes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id)
            {
                AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(MainActivity.this);
                dialogBuilder.setTitle(getString(R.string.alert_choose_title));
                dialogBuilder.setMessage(getString(R.string.choose_message));
                dialogBuilder.setCancelable(false);
                dialogBuilder.setPositiveButton(R.string.modify, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        modify(position);
                    }
                });
                dialogBuilder.setNegativeButton(R.string.remove, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        removeNote(position);
                    }
                });
                dialogBuilder.setNeutralButton(R.string.exit, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog=dialogBuilder.create();
                dialog.show();
                return false;
            }
        });
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        FirebaseUser user=auth.getCurrentUser();
        if(user!=null) cloud=true;
    }

    private void readaptListView(ArrayList<Note> noteArray)
    {
        noteAdapter.setList(noteArray);
        notes.setAdapter(noteAdapter);
    }

    private void modify(final int position)
    {
        LayoutInflater inflater=getLayoutInflater().from(MainActivity.this);
        View view=inflater.inflate(R.layout.input_dialog,null);
        AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(MainActivity.this);
        dialogBuilder.setView(view);
        dialogBuilder.setTitle(R.string.modify);
        dialogBuilder.setCancelable(false);
        final SpeakView desc = (speechEdit == null) ? (SpeakView) getSupportFragmentManager().findFragmentById(R.id.desc) : speechEdit;
        desc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                try
                {
                    //speechEdit=desc;
                    startActivityForResult(intent,CAPTURE_SPEECH);
                }
                catch (ActivityNotFoundException e)
                {
                    Toast.makeText(getApplicationContext(),R.string.not_available,Toast.LENGTH_SHORT).show();
                }
            }
        });
        final CalendarView calendar=view.findViewById(R.id.deadline);
        desc.setHint(noteList.get(position).getDescription());
        dialogBuilder.setPositiveButton(R.string.modify, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                try
                {
                    Note note=noteList.get(position);
                    if(fireNotes.contains(note)) modifyFireStore(note,desc.getText().toString(),calendar.getDate());
                    else modifyDatabase(note,desc.getText().toString(),calendar.getDate());
                    Toast.makeText(getApplicationContext(), R.string.note_modified, Toast.LENGTH_SHORT).show();
                }
                catch (IllegalArgumentException e)
                {
                    Toast.makeText(MainActivity.this,R.string.query_failed,Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setNeutralButton(R.string.exit, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        ImageButton shoot=view.findViewById(R.id.shoot);
        shoot.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                actionPos=position;
                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(intent.resolveActivity(getPackageManager())!=null)
                    startActivityForResult(intent,CAPTURE_IMAGE);
            }
        });
        ImageButton register=view.findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                actionPos=position;
                Intent intent=new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
                startActivityForResult(intent,CAPTURE_AUDIO);
            }
        });
        AlertDialog dialog=dialogBuilder.create();
        dialog.show();
    }

    private void modifyFireStore(Note note, String toString, long date)
    {
        //store.collection("notes").
    }

    private void modifyDatabase(Note note, String desc, long date)
    {
        Cursor cursor = manager.get();
        boolean end = false;
        int index = cursor.getColumnIndexOrThrow(DatabaseContract.DESC);
        for (int i = 0; i < cursor.getCount() && !end; i++)
        {
            cursor.moveToPosition(i);
            String value = cursor.getString(index);
            if (value.equals(note.getDescription()))
                end = true;
        }
        if(!end) throw new IllegalArgumentException();
        index = cursor.getColumnIndexOrThrow(DatabaseContract.ID);
        long id = cursor.getLong(index);
        //Toast.makeText(getApplicationContext(),newDeadline.toString(),Toast.LENGTH_SHORT).show();
        if (!desc.equals("")) note.setDescription(desc);
        note.setDeadline(new Date(date));
        readaptListView(noteList);
        manager.update(id, note.getDescription(), note.getDeadline(), note.getPosition(), note.getImage(), note.getAudio());
    }

    private void removeNote(final int position)
    {
        AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(MainActivity.this);
        dialogBuilder.setTitle(getString(R.string.remove));
        dialogBuilder.setMessage(getString(R.string.remove_message));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Toast.makeText(getApplicationContext(),R.string.note_removed,Toast.LENGTH_SHORT).show();
                Cursor cursor=manager.get();
                cursor.moveToFirst();
                boolean end=false;
                int index=0;
                while(!end && !cursor.isClosed())
                {
                    index=cursor.getColumnIndexOrThrow(DatabaseContract.DESC);
                    if(cursor.getString(index).equals(noteList.get(position).getDescription()))
                        end=true;
                    else cursor.moveToNext();
                }
                index=cursor.getColumnIndexOrThrow(DatabaseContract.ID);
                long id=cursor.getLong(index);
                noteList.remove(position);
                manager.remove(id);
                readaptListView(noteList);
            }
        });
        dialogBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        AlertDialog dialog=dialogBuilder.create();
        dialog.show();

    }

    public void addNote()
    {
        LayoutInflater inflater=getLayoutInflater().from(MainActivity.this);
        View view=inflater.inflate(R.layout.input_dialog,null);
        AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(MainActivity.this);
        dialogBuilder.setView(view);
        dialogBuilder.setTitle(R.string.add);
        dialogBuilder.setCancelable(false);
        final SpeakView desc = speechEdit == null ? (SpeakView) getSupportFragmentManager().findFragmentById(R.id.desc) : speechEdit;
        final CalendarView calendar=view.findViewById(R.id.deadline);
        desc.setHint(R.string.insert_desc);
        desc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                try
                {
                    speechEdit=desc;
                    startActivityForResult(intent,CAPTURE_SPEECH);
                }
                catch (ActivityNotFoundException e)
                {
                    Toast.makeText(getApplicationContext(),R.string.not_available,Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogBuilder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                try
                {
                    if(cloud) addFire(desc.getText().toString(),calendar.getDate(),image,audio);
                    else addDatabase(desc.getText().toString(),calendar.getDate(),image,audio);
                    /*String descString=desc.getText().toString();
                    Date date=new Date(calendar.getDate());
                    if(descString.equals("")) throw new IllegalArgumentException();
                    LocationTrack track=new LocationTrack(MainActivity.this);
                    LatLng position=new LatLng(track.getLatitude(),track.getLongitude());
                    Note note=new Note(descString,date,position);
                    note.setImage(image);
                    note.setAudio(audio);
                    manager.add(note.getDescription(),note.getDeadline(),note.getPosition(),note.getImage(),note.getAudio());
                    noteList.add(note);
                    readaptListView(noteList);
                    Toast.makeText(getApplicationContext(),R.string.note_added,Toast.LENGTH_SHORT).show();*/
                }
                catch (IllegalArgumentException e)
                {
                    Toast.makeText(getApplicationContext(),R.string.desc_error,Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(getApplicationContext(),R.string.note_modified,Toast.LENGTH_SHORT).show();
            }
        });
        dialogBuilder.setNeutralButton(R.string.exit, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        ImageButton shoot=view.findViewById(R.id.shoot);
        shoot.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,CREATE_IMAGE);
            }
        });
        ImageButton register=view.findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
                startActivityForResult(intent,CREATE_AUDIO);
            }
        });
        AlertDialog dialog=dialogBuilder.create();
        dialog.show();
    }

    private void addDatabase(String desc, long date, Bitmap image, MediaStore.Audio audio)
    {
        if(desc.equals("")) throw new IllegalArgumentException();
        LocationTrack track=new LocationTrack(MainActivity.this);
        LatLng position=new LatLng(track.getLatitude(),track.getLongitude());
        Note note=new Note(desc,new Date(date),position);
        note.setImage(image);
        note.setAudio(audio);
        manager.add(note.getDescription(),note.getDeadline(),note.getPosition(),note.getImage(),note.getAudio());
        noteList.add(note);
        readaptListView(noteList);
        Toast.makeText(getApplicationContext(),R.string.note_added,Toast.LENGTH_SHORT).show();
    }

    private void addFire(String desc, long date, Bitmap image, MediaStore.Audio audio)
    {
        Map<String,Object> noteMap=new HashMap<String,Object>();
        noteMap.put("desc",desc);
        noteMap.put("deadline",date);
        //ByteBuffer buffer=ByteBuffer.allocate(image.getByteCount());
        //image.copyPixelsToBuffer(buffer);
        //buffer.rewind();
        //noteMap.put("image",buffer.array());
        //noteMap.put("audio",audio);
        LocationTrack track=new LocationTrack(MainActivity.this);
        LatLng position=new LatLng(track.getLatitude(),track.getLongitude());
        noteMap.put("position",position);
        store.collection("notes").add(noteMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>()
                {
                    @Override
                    public void onSuccess(DocumentReference documentReference)
                    {
                        Toast.makeText(MainActivity.this,R.string.note_added,Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        Toast.makeText(MainActivity.this,R.string.error,Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE)
        {
            if (resultCode == RESULT_OK)
            {
                Note note=noteList.get(actionPos);
                note.setImage((Bitmap) data.getExtras().get("data"));
                Toast.makeText(getApplicationContext(),R.string.image_added,Toast.LENGTH_SHORT).show();
            }
            else if (resultCode == RESULT_CANCELED)
                Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_LONG).show();
        }
        else if(requestCode == CAPTURE_AUDIO)
        {
            if (resultCode == RESULT_OK)
            {
                Note note=noteList.get(actionPos);
                note.setAudio((MediaStore.Audio) data.getExtras().get("data"));
                Toast.makeText(getApplicationContext(),R.string.audio_added,Toast.LENGTH_SHORT).show();
            }
            else if (resultCode == RESULT_CANCELED)
                Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_LONG).show();
        }
        else if(requestCode == CREATE_IMAGE)
        {
            if (resultCode == RESULT_OK)
            {
                image=(Bitmap) data.getExtras().get("data");
                Toast.makeText(getApplicationContext(),R.string.image_added,Toast.LENGTH_SHORT).show();
            }
            else if (resultCode == RESULT_CANCELED)
                Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_LONG).show();
        }
        else if(requestCode == CREATE_AUDIO)
        {
            if (resultCode == RESULT_OK)
            {
                audio=(MediaStore.Audio) data.getExtras().get("data");
                Toast.makeText(getApplicationContext(),R.string.audio_added,Toast.LENGTH_SHORT).show();
            }
            else if (resultCode == RESULT_CANCELED)
                Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_LONG).show();
        }
        else if(requestCode == CAPTURE_SPEECH)
        {
            if (resultCode == RESULT_OK)
            {
                ArrayList<String> result=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                speechEdit.setText(result.get(0));
                //speechEdit=null;
            }
            else if (resultCode == RESULT_CANCELED)
                Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInit(int status)
    {
        if(status==TextToSpeech.SUCCESS)
        {
            int result=speech.setLanguage(Locale.getDefault());
            if(result==TextToSpeech.LANG_MISSING_DATA || result==TextToSpeech.LANG_NOT_SUPPORTED)
                Log.e(getString(R.string.error),getString(R.string.language_not_supposed));
        }
        else Log.e(getString(R.string.error),getString(R.string.init_fail));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case(R.id.map): showMap(); break;
            case(R.id.login): login(); break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    private void showMap()
    {
        Intent intent=new Intent(MainActivity.this,MapsActivity.class);
        String desc[]=new String[noteList.size()];
        Date deadline[]=new Date[noteList.size()];
        Parcelable image[]=new Parcelable[noteList.size()];
        String pos[]=new String[noteList.size()];
        MediaStore.Audio audio[]=new MediaStore.Audio[noteList.size()];
        for(int i=0;i<noteList.size();i++)
        {
            Note note=noteList.get(i);
            desc[i]=note.getDescription();
            deadline[i]=note.getDeadline();
            image[i]=note.getImage();
            audio[i]=note.getAudio();
            pos[i]=note.getPosition().toString();
        }
        intent.putExtra(getString(R.string.desc),desc);
        intent.putExtra(getString(R.string.deadline),deadline);
        intent.putExtra(getString(R.string.image),image);
        intent.putExtra(getString(R.string.audio),audio);
        intent.putExtra(getString(R.string.pos),pos);
        startActivity(intent);
    }

    private void login()
    {
        Intent intent=new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
    }
}
