package exam.springlab.todolist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback
{
    private GoogleMap mMap;
    private ArrayList<Note> notes;
    private HashMap<Marker,Note> noteMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        notes=new ArrayList<Note>();
        String desc[]= (String[]) bundle.get(getString(R.string.desc));
        Date date[]= (Date[]) bundle.get(getString(R.string.deadline));
        Parcelable image[]= (Parcelable[]) bundle.get(getString(R.string.image));
        MediaStore.Audio audio[]= (MediaStore.Audio[]) bundle.get(getString(R.string.audio));
        String pos[]= (String[]) bundle.get(getString(R.string.pos));
        noteMarker=new HashMap<Marker,Note>();
        for(int i=0;i<desc.length;i++)
        {
            Note note=new Note(desc[i],date[i],parse(pos[i]));
            if(image[i]==null) Log.e("Image","NULL");
            else note.setImage((Bitmap) image[i]);
            note.setAudio(audio[i]);
            notes.add(note);
        }
        for(Note note:notes)
        {        // Add a marker in Sydney and move the camera
            MarkerOptions markerOptions=new MarkerOptions();
            markerOptions.position(note.getPosition());
            markerOptions.title(note.getDeadline().toString());
            markerOptions.snippet(note.getDescription());
            //markerOptions.
            Marker marker=mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(note.getPosition()));
            noteMarker.put(marker,note);
        }
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener()
        {
            @Override
            public void onInfoWindowClick(Marker marker)
            {
                Intent intent=new Intent(MapsActivity.this,NoteActivity.class);
                intent.putExtra(getString(R.string.desc),marker.getSnippet());
                intent.putExtra(getString(R.string.deadline),marker.getTitle());
                Note note=noteMarker.get(marker);
                intent.putExtra(getString(R.string.image),note.getImage());
                //intent.putExtra(getString(R.string.audio),note.getAudio());
                startActivity(intent);

            }
        });
    }



    private LatLng parse(String pos)
    {

        String[] set=pos.substring(10,pos.length()-1).split(",");
        double lat=(float) Double.parseDouble(set[0]);
        double lng=(float) Double.parseDouble(set[1]);
        return new LatLng(lat,lng);
        //return null;
    }
}
