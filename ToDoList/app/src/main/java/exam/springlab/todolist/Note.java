package exam.springlab.todolist;

import android.graphics.Bitmap;
import android.provider.MediaStore;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Note
{
    private int id;
    private String description;
    private Date deadline;
    private Bitmap image;
    private MediaStore.Audio audio;
    private LatLng position;

    public Note(String description, Date deadline, LatLng position)
    {
        this.description = description;
        this.deadline = deadline;
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public MediaStore.Audio getAudio() {
        return audio;
    }

    public void setAudio(MediaStore.Audio audio) {
        this.audio = audio;
    }

    public LatLng getPosition()
    {
        return position;
    }

}
