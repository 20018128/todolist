package exam.springlab.todolist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;


public class SpeakView extends Fragment
{
    // TODO: Rename and change types of parameters
    private EditText edit;
    private ImageButton button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        final View view=inflater.inflate(R.layout.speak_view,container,false);
        edit=view.findViewById(R.id.edit);
        button=view.findViewById(R.id.button);
        return view;
    }

    public Editable getText()
    {
        return edit.getText();
    }

    public void setText(String text)
    {
        edit.setText(text);
    }

    public void setText(int stringId)
    {
        setText(getString(stringId));
    }

    public void setHint(String hint)
    {
        edit.setHint(hint);
    }

    public void setHint(int hintId)
    {
        setHint(getString(hintId));
    }

    public CharSequence getHint()
    {
        return edit.getHint();
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        button.setOnClickListener(listener);
    }

}
