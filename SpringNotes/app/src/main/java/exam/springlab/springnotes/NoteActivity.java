package exam.springlab.todolist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class NoteActivity extends AppCompatActivity
{
    private TextView within, desc;
    private Button show, listen;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        within=findViewById(R.id.within);
        desc=findViewById(R.id.desc);
        show=findViewById(R.id.show_image);
        listen=findViewById(R.id.listen_audio);
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        String[] dateData=bundle.get(getString(R.string.deadline)).toString().split(" ");
        String dateString=getString(R.string.remember_to)+" "+dateData[0]+" "+dateData[1]+" "+dateData[2]+" "+dateData[5];
        within.setText(dateString);
        desc.setText(bundle.getString(getString(R.string.desc)));
    }
}
