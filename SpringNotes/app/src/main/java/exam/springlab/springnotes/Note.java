package exam.springlab.todolist;

import android.graphics.Bitmap;
import android.provider.MediaStore;

import java.util.Date;

public class Note
{
    String description;
    Date deadline;

    Bitmap image;
    MediaStore.Audio audio;
   // LatLng position;

    public Note(String description, Date deadline/*, LatLng position*/)
    {
        this.description = description;
        this.deadline = deadline;
       // this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public MediaStore.Audio getAudio() {
        return audio;
    }

    public void setAudio(MediaStore.Audio audio) {
        this.audio = audio;
    }

    /*public LatLng getPosition() {
        return position;
    }*/

}
